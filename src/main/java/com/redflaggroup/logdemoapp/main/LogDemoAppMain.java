package com.redflaggroup.logdemoapp.main;



import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.ClassPathXmlApplicationContext;



public class LogDemoAppMain {

    private static final Logger logger = LoggerFactory.getLogger( LogDemoAppMain.class );
    /*
     * Client code does not need to depend on logback. Since SLF4J permits the use of any logging framework under 
     * its abstraction layer, it is easy to migrate large bodies of code from one logging framework to another.
     */


    @Value( "${db.connstring.url}" )
    private String dbConnStringUrl;


    public static void main( String[] args ) {

        logger.info( "################################################################################" );
        logger.info( "################################################################################" );
        logger.info( "START : LogDemoAppMain" );

        ClassPathXmlApplicationContext appCtx = new ClassPathXmlApplicationContext( "logDemoApp-context.xml" );
        appCtx.registerShutdownHook();

        LogDemoAppMain logDemoAppMain = (LogDemoAppMain) appCtx.getBean( "logDemoAppMain"  );

        logger.error( "" );
        logger.error( "dbConnStringUrl = '{}'", logDemoAppMain.dbConnStringUrl );
        logger.error( "" );

        try {

            SimpleDateFormat sdf         = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss.SSS" );
            Date             progEndDate = sdf.parse( "2016-11-15 17:00:00.000" );
            Date             now         = null; // Calendar.getInstance().getTime()
            long             i           = 0;

            while ( (now=Calendar.getInstance().getTime()).compareTo(progEndDate) < 0 ) {

                logger.trace( String.format("%6.6s",i+")") +" "+ String.format("%-5.5s","TRACE") +" - ["+ sdf.format(now) +"]" );
                logger.debug( String.format("%6.6s",i+")") +" "+ String.format("%-5.5s","DEBUG") +" - ["+ sdf.format(now) +"]" );
                logger.info(  String.format("%6.6s",i+")") +" "+ String.format("%-5.5s","INFO" ) +" - ["+ sdf.format(now) +"]" );
                logger.warn(  String.format("%6.6s",i+")") +" "+ String.format("%-5.5s","WARN" ) +" - ["+ sdf.format(now) +"]" );
                if ( i % 4320 == 0 ) {
                    try {
                        throw new Exception( "Ooops!... Exception in program." );
                    }
                    catch ( Exception x ) {
                        logger.error( String.format("%6.6s",i+")") +" "+ String.format("%-5.5s","ERROR") +" - ["+ sdf.format(now) +"] -> '{}'", x.getMessage(), x );
                    }
                }
                logger.info( "--------------------------------------------------------------------------------" );

                Thread.sleep( 60000 );
                i++;
                
                break;

                // now = Calendar.getInstance().getTime();
            }
        }
        catch ( ParseException | InterruptedException e ) {
            // e.printStackTrace();
            logger.error( "ERROR-LogDemoAppMain: Message = '{}'", e.getMessage(), e );
        }

        // Close resources
        appCtx.close();

        logger.info( "END   : LogDemoAppMain" );
        logger.info( "================================================================================" );
        logger.info( "" );

    }

}
